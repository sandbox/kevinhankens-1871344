#Sweet module, man

This module is a simple script container. Basically, it lets you enter some javascript via a Drupal config page. That JS will be included in the footer of your site, thus initiating the sweet analytics. 

#Installation

Just do it the normal way. Nothing special.

#Config

Using your Drupal site, navigate to /admin/config/system/sweet and copy-and-paste the example js into the textarea. Using the {%DRUPAL_DROP%} token inside the self-calling JS function, you can allow Drupal to insert some knowledge that it has of the environment, session, etc. For now, it's all contained in the sweet_knowledge_drop() function, but could be extended a bit further in the future.
