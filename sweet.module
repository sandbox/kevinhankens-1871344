<?php

/**
 * Implements hook_init().
 */
function sweet_init() {
  drupal_add_js(
    sweet_get_js(TRUE),
    array('group' => JS_THEME, 'type' => 'inline', 'scope' => 'footer', 'weight' => 9999, 'sweet' => TRUE)
  );  
}

/**
 * Implements hook_js_alter().
 *
 * Ensure that our js is last in line.
 */
function sweet_js_alter(&$js) {
  $sweet_key = NULL;
  $max_weight = 0;
  foreach ($js as $key => $script) {
    if (!empty($script['sweet'])) {
      $sweet_key = $key;
    }
    elseif($script['weight'] > $max_weight) {
      $max_weight = $script['weight'] + 1;
    }
  }

  if ($sweet_key) {
    $js[$sweet_key]['weight'] = $max_weight;
  }
}

/**
 * Implements hook_menu().
 */
function sweet_menu() {
  return array(
    'admin/config/system/sweet' => array(
      'type' => MENU_NORMAL_ITEM,
      'title' => 'Sweet settings',
      'description' => 'Configure sweet server and client settings and stuff like that.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('sweet_admin'),
      'access arguments' => array('access sweet admin'),
    ),
  );
}

/**
 * Implements hook_permission().
 */
function sweet_permission() {
  return array(
    'access sweet admin' => array(
      'title' => 'Access sweet settings',
    ),
  );
}

/**
 * Administration form.
 */
function sweet_admin() {
  $form = array();

  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => 'Some sweet settings',
  );

  $form['sweet_js'] = array(
    '#type' => 'textarea',
    '#title' => t('Footer Javascript - do not include &lt;script&gt; and &lt;/script&gt; tags.'),
    '#default_value' => sweet_get_js(),
    '#description' => 'Use {%DRUPAL_DROP%} inside the self calling function to drop some php knowledge. Use the following script as an example.',
  );

  $form['sweet_default'] = array(
    '#type' => 'markup',
    '#prefix' => '<div><pre>',
    '#suffix' => '</pre></div>',
    '#markup' => htmlspecialchars(sweet_get_js(FALSE, TRUE)),
  );

  return system_settings_form($form);
}

/**
 * Helper function to retrieve.
 *
 * @param Boolean $transpose
 *  Optional. Whether or not to replace the {%DRUPAL_DROP%} token.
 * @param Boolean $get_default
 *  Optional. Retreive a sane default value.
 */
function sweet_get_js($transpose = FALSE, $get_default = FALSE) {
  $js = variable_get('sweet_js', NULL);

  $default = <<<EOD
jQuery(document).ready(function() {
  var _sweet = _sweet || [];
  var t   = document.createElement('script');
  t.type  = 'text/javascript';
  t.async = true;
  t.id    = 'sweet';
  t.setAttribute('site_id', 'site_id_here');
  {%DRUPAL_DROP%}
  t.src = 'http://sweet.acquia.com/ping.js';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(t, s);
});
EOD;

  if ($get_default) {
    return $default; 
  }

  if (!$js) {
    $js = $default;
    variable_set('sweet_js', $js);
  }

  if ($transpose) {
    sweet_knowledge_drop($js);
  }

  return $js;
}

/**
 * Inserts some knowledge that PHP/Drupal has about the session
 * environment, etc.
 * 
 * @param String $js
 *  The user-configured Javascript
 */
function sweet_knowledge_drop(&$js) {
  $knowledge = array();
  if (!user_is_anonymous()) {
    $knowledge['session_id'] = session_id();
  }
  if ($environment = variable_get('environment_indicator_text', FALSE)) {
    $knowledge['environment_id'] = $environment;
  }

  $drop = '';
  foreach ($knowledge as $key => $value) {
    $drop .= "t.setAttribute('$key', '$value');\n  ";
  }
  $js = preg_replace('@\{%\s*DRUPAL_DROP\s*%\}@', $drop, $js);
}
